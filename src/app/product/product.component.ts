import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Product} from './product'


@Component({
  selector: 'jce-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['product']
})
export class ProductComponent implements OnInit {

  @Output() deleteEvent = new EventEmitter<Product>();
  @Output() editEvent = new EventEmitter<Product>();

  product:Product;
  tempProduct:Product = {Pid:null,Pname:null,Cost:null,Categoryid:null};
  isEdit : boolean = false;
  editButtonText = 'Edit';
  
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.product);
  }

  cancelEdit(){
    this.isEdit = false;
    this.product.Pid = this.tempProduct.Pid;
    this.product.Pname = this.tempProduct.Pname;
     this.product.Cost = this.tempProduct.Cost;
    this.product.Categoryid = this.tempProduct.Categoryid;
    this.editButtonText = 'Edit'; 
  }

  toggleEdit(){
    //update parent about the change
    this.isEdit = !this.isEdit; 
    this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
     
     if(this.isEdit){
       this.tempProduct.Pid = this.product.Pid;
      this.tempProduct.Pname = this.product.Pname;
       this.tempProduct.Cost = this.product.Cost;
       this.tempProduct.Categoryid = this.product.Categoryid;
     } else {     
       this.editEvent.emit(this.product);
     } 
  }

  ngOnInit() {
  }

}
