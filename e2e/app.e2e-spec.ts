import { IsraelPage } from './app.po';

describe('israel App', function() {
  let page: IsraelPage;

  beforeEach(() => {
    page = new IsraelPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
